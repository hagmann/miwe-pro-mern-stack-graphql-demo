import { GraphQLScalarType } from 'graphql'
import { Kind } from 'graphql/language'

export const GraphQLDate = new GraphQLScalarType({
  name: 'GraphQLDate',
  description: 'A Date() type in GraphQL as a scalar',
  parseValue (value) {
    return new Date(value)
  },
  parseLiteral (ast) {
    return ast.kind === Kind.STRING ? new Date(ast.value) : undefined
  },
  serialize (value) {
    return value.toISOString()
  }
    })
