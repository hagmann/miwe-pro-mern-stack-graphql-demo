import { issues } from '../utils/db.mjs'

export const issueAdd = (_, { issue }) => {
  issue.created = new Date()
  issue.id = issues.length + 1
  if (issue.status === undefined) issue.status = 'New'

  issues.push(issue)
  return issue
}

export const issueList = () => issues
