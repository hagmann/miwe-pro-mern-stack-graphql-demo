import gql from 'graphql-tag'
import { issueAdd, issueList } from '../data/issues.mjs'

export const typeDef = gql`
  # extend = add fields to already defined type #
  extend type Query {
    issueList: [Issue!]!
  }

  # extend = add fields to already defined type #
  extend type Mutation {
    issueAdd(issue: IssueInputs!): Issue!
  }

  type Issue {
    id: Int!
    title: String!
    status: String!
    owner: String
    effort: Int
    created: GraphQLDate!
    due: GraphQLDate
  }

  input IssueInputs {
    title: String!
    status: String
    owner: String
    effort: Int
    due: GraphQLDate
  }
`

export const resolvers = {
  Query: {
    issueList: () => issueList()
  },
  Mutation: {
    issueAdd
  }
}
