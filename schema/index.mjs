import gql from 'graphql-tag'
import _ from 'lodash'
import { makeExecutableSchema } from 'apollo-server-express'
import { typeDef as About, resolvers as aboutResolvers } from './about.mjs'
import { typeDef as Issue, resolvers as issueResolvers } from './issue.mjs'

// General GraphQL types
const Query = gql`
  scalar GraphQLDate

  # GraphQL requires any type to have at least one field, so we add a fake one here #
  type Query {
    _empty: String
  }

  # GraphQL requires any type to have at least one field, so we add a fake one here #
  type Mutation {
    _empty: String
  }
`

const resolvers = {}

export default makeExecutableSchema({
  typeDefs: [Query, About, Issue],
  resolvers: _.merge(resolvers, aboutResolvers, issueResolvers)
})
