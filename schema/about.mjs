import gql from 'graphql-tag'
import { getAboutMessage, setAboutMessage } from '../data/about.mjs'

export const typeDef = gql`
  # extend = add fields to already defined type #
  extend type Query {
    about: String!
  }

  # extend = add fields to already defined type #
  extend type Mutation {
    setAboutMessage(message: String!): String
  }
`

export const resolvers = {
  Query: {
    about: getAboutMessage
  },
  Mutation: {
    setAboutMessage
  }
}
