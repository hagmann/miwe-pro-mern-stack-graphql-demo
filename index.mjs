import express from 'express'
import { ApolloServer } from 'apollo-server-express'
import schema from './schema/index.mjs'

const app = express()

const server = new ApolloServer({ schema })

server.applyMiddleware({ app, path: '/graphql' })

app.listen(3000, () => console.log('App started'))
